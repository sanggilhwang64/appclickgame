import 'package:app_click_game/model/game_artifact_item.dart';

final List<GameArtifactItem> configArtifacts = [
  GameArtifactItem('노말', '가죽 모자', 'leather_helmet.jpg', 17.5, 0),
  GameArtifactItem('노말', '가죽 갑옷', "leather_armor.jpg", 17.5, 0),
  GameArtifactItem('노말', '가죽 신발', "leather_shoes.jpg", 17.5, 0 ),
  GameArtifactItem('노말', '목검', "wood_sword.jpg", 17.5, 0),
  GameArtifactItem('레어', '철 모자', "iron_helmet.jpg", 5 , 15 ),
  GameArtifactItem('레어', '철 갑옷', "iron_armor.jpg", 5 , 15 ),
  GameArtifactItem('레어', '철 신발', "iron_shoes.jpg", 5 , 15),
  GameArtifactItem('레어', '철 검', "iron_sword.jpg", 5, 15 ),
  GameArtifactItem('에픽', '은 모자', "silver_helmet.jpg", 2 , 8.75 ),
  GameArtifactItem('에픽', '은 갑옷', "silver_armor.jpg", 2 , 8.75),
  GameArtifactItem('에픽', '은 신발', "silver_shoes.jpg", 2 , 8.75 ),
  GameArtifactItem('에픽', '은 검', "silver_sword.jpg", 2 , 8.75 ),
  GameArtifactItem('전설', '금 모자', "gold_helmet.jpg", 0.5 , 1 ),
  GameArtifactItem('전설', '금 갑옷', "gold_armor.jpg", 0.5 , 1 ),
  GameArtifactItem('전설', '금 신발', "gold_shoes.jpg", 0.5 , 1 ),
  GameArtifactItem('전설', '금 검', "gold_sword.jpg", 0.5 , 1),
  GameArtifactItem('신화', '다이아몬드 모자', "diamond_helmet.jpg", 0 , 0.25 ),
  GameArtifactItem('신화', '다이아몬드 갑옷', "diamond_armor.jpg", 0 , 0.25 ),
  GameArtifactItem('신화', '다이아몬드 신발', "diamond_shoes.jpg", 0 , 0.25),
  GameArtifactItem('신화', '다이아몬드 검', "diamond_sword.jpg", 0 , 0.25 ),
];

