import 'dart:ui';

import 'package:flutter/material.dart';

class ComponentArtifactInfo extends StatelessWidget {
  const ComponentArtifactInfo(
      {super.key,
        required this.imageName,
        required this.ratingName,
        required this.artifactName,
        required this.artifactCount,
        }
      );

  final String imageName;
  final String ratingName;
  final String artifactName;
  final int artifactCount;


  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width / 4,
            height: 80,
            child: _getImage(),
          ),
          artifactCount == 0 ? Text('--') : _getRatingBadge(),
          Text(artifactCount == 0 ? '--' : artifactName),
          Text(artifactCount == 0 ? '--' : artifactCount.toString()),
        ],
      ),
    );
  }

  Widget _getRatingBadge() {
    switch(ratingName) {
      case '노말':
        return const Chip(
            label: Text(
              '노말',
              style: TextStyle(color: Colors.white),
            ),
          backgroundColor: Colors.grey,
        );
      case '레어':
        return const Chip(
          label: Text(
            '레어',
            style: TextStyle(color: Colors.white),
          ),
          backgroundColor: Colors.green,
        );
      case '에픽':
        return const Chip(
          label: Text(
            '노말',
            style: TextStyle(color: Colors.white),
          ),
          backgroundColor: Colors.blue,
        );
      case '전설':
        return const Chip(
          label: Text(
            '노말',
            style: TextStyle(color: Colors.white),
          ),
          backgroundColor: Colors.yellow,
        );
      default:
        return const Chip(
          label: Text(
            '노말',
            style: TextStyle(color: Colors.white),
          ),
          backgroundColor: Colors.red,
        );
    }
  }

  Widget _getImage() {
    if (artifactCount == 0) {
      return ImageFiltered(
        imageFilter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
        child: Image.asset(
          'assets/${imageName}',
        ),
      );
    } else {
      return Image.asset(
        'assets/${imageName}',
      );
    }
  }
}
