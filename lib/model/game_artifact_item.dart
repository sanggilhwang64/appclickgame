class GameArtifactItem {
  String rating;
  String artifactName;
  String imageName;
  double silverPercent;
  double goldPercent;

  GameArtifactItem(this.rating, this.artifactName, this.imageName, this.silverPercent, this.goldPercent);
}