import 'dart:math';

import 'package:app_click_game/model/choose_artifact_item.dart';
import 'package:app_click_game/model/game_artifact_item.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../config/config_init_data.dart';

class PageInGame extends StatefulWidget {
  const PageInGame({Key? key}) : super(key: key);

  @override
  State<PageInGame> createState() => _PageInGameState();
}

class _PageInGameState extends State<PageInGame> {
  int _money = 0;
  List<String> _useArtifacts = [];

  @override
  void initState() {
    super.initState();
    _initMoney();
    _getUseArtifacts();
  }

  /// 보유 돈 정보 가져오기. 처음 실행이라면 돈은 0원
  void _initMoney() async {
    final prefs = await SharedPreferences.getInstance();
    int oldMoney = prefs.getInt('money') ?? 0;
    setState(() {
      _money = oldMoney;
    });
  }

  /// 돈 정보 저장
  void _saveMoney() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setInt('money', _money);
  }

  /// 탭시 돈 증가
  void _digUpMoney() {
    setState(() {
      _money += 1000;
    });
  }

  /// 보유 유물 정보 가져오기
  void _getUseArtifacts() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      _useArtifacts = prefs.getStringList('useArtifacts') == null ? [] : prefs.getStringList('useArtifacts')!;
    });
  }

  /// 유물 뽑기
  void _getChooseResult(bool isGoldBox) {
    int choosePay = isGoldBox ? 30000 : 5000;
    bool isEnoughMoney = _isStartChooseByMoneyCheck(choosePay);

    if (isEnoughMoney) {
      setState(() {
        _money -= choosePay;
      });
      
      int artifactResultId = _getChooseArtifact(isGoldBox);
      
      bool isNewArtifact = false;
      int chooseArtifactCount = int.parse(_useArtifacts[artifactResultId]);
      if (chooseArtifactCount == 0) isNewArtifact = true;

      _plusUseArtifact(artifactResultId, chooseArtifactCount);
      _getUseArtifacts();

      _dialogArtifact(isGoldBox, artifactResultId, isNewArtifact);
    }
  }

  void _plusUseArtifact(int index, int oldCount) async {
    List<String> useArtifactTemp = _useArtifacts;
    _useArtifacts[index] = (oldCount + 1).toString();

    final prefs = await SharedPreferences.getInstance();
    prefs.setStringList('useArtifacts', useArtifactTemp);
  }

  bool _isStartChooseByMoneyCheck(int choosePay) {
    bool result = false;

    if (_money >= choosePay) result = true;

    return result;
  }


  int _getChooseArtifact(bool isGoldBox) {
    List<GameArtifactItem> artifacts = configArtifacts;
    
    List<ChooseArtifactItem> percentBar = [];
    
    double oldPercent = 0;
    int index = 0;
    for (GameArtifactItem artifact in artifacts) {
      ChooseArtifactItem addItem = ChooseArtifactItem(
          index,
          oldPercent,
          isGoldBox ? oldPercent + artifact.goldPercent : oldPercent + artifact.silverPercent);
      percentBar.add(addItem);

      if (isGoldBox) {
        oldPercent += artifact.goldPercent;
      } else {
        oldPercent += artifact.silverPercent;
      }

      index++;
    }

    double percentResult = Random().nextDouble() * 100;

    int resultId = 0;
    for (ChooseArtifactItem item in percentBar) {
      if (percentResult >= item.percentMin && percentResult <= item.percentMax) {
        resultId = item.id;
        break;
      }
    }

    return resultId;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width,
            height: 250,
            child: Image.asset('assets/stone.jpeg', fit: BoxFit.fill,),
          ),
          const SizedBox(
            height: 10,
          ),
          ElevatedButton(
              onPressed: _digUpMoney,
              child: Text('캐기'),
          ),
          const SizedBox(
            height: 20,
          ),
          Text('보유한 돈 : ${_money}'
          ),
          const SizedBox(
            height: 20,
          ),
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              SizedBox(
                width: MediaQuery.of(context).size.width / 4,
                height: 100,
                child: Image.asset('assets/silver_box.PNG'),
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width / 4,
                child: ElevatedButton(
                  onPressed: () {
                    _getChooseResult(false);
                  },
                  child: const Text("은상자 뽑기 (-5000)", style: TextStyle(fontSize: 10.0),
                  ),
                ),
              ),
            ],
          ),
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              SizedBox(
                width: MediaQuery.of(context).size.width / 4,
                height: 100,
                child: Image.asset('assets/gold_box.png',),
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width / 4,
                child: ElevatedButton(
                  onPressed: () {
                    _getChooseResult(true);
                  },
                  child: const Text('금상자 뽑기 (-30000)', style: TextStyle(fontSize: 10.0),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Future<void> _dialogArtifact(bool isGoldBox, int artifactId, bool isNew) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(isGoldBox ? '금상자를 봅았습니다.' : '은상자를 뽑았습니다.',
            textAlign: TextAlign.center,
          ),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                SizedBox(
                  width: 100,
                  height: 100,
                  child: Image.asset('assets/${configArtifacts[artifactId].imageName}'),
                ),
                const SizedBox(
                  height: 30,
                ),
                Text('${configArtifacts[artifactId].artifactName}을 뽑았습니다.', textAlign: TextAlign.center,),
                const SizedBox(
                  height: 30,
                ),
                Text(isNew ? '새로운 유물!' : '보유중', textAlign: TextAlign.center,),
              ],
            ),
          ),
          actions: [
            TextButton(
              child: Text('확인'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      }
    );
  }
  @override
  void dispose() {
    _saveMoney();
    super.dispose();
  }
}
