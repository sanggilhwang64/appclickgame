import 'package:app_click_game/components/component_artifact_info.dart';
import 'package:app_click_game/config/config_init_data.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PageArtifactList extends StatefulWidget {
  const PageArtifactList({Key? key}) : super(key: key);

  @override
  State<PageArtifactList> createState() => _PageArtifactListState();
}

class _PageArtifactListState extends State<PageArtifactList> {
  List<String> _useArtifacts = [];

  void _getUseArtifacts() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      _useArtifacts = prefs.getStringList('useArtifacts') == null ? [] : prefs.getStringList('useArtifacts')!;
    });
  }

  @override
  void initState() {
    super.initState();

    _getUseArtifacts();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _useArtifacts.length == 0 ? Container() : ListView.builder(
        itemCount: configArtifacts.length,
        itemBuilder: (BuildContext ctx, int idx) {
          return ComponentArtifactInfo(
              imageName: configArtifacts[idx].imageName,
              ratingName: configArtifacts[idx].rating,
              artifactName: configArtifacts[idx].artifactName,
              artifactCount: int.parse(_useArtifacts[idx])
          );
        },
      ),
    );
  }
}
